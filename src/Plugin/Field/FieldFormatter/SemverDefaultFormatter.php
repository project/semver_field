<?php

namespace Drupal\semver_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityDisplayBase;

/**
 * Plugin implementation of the 'semver_default' formatter.
 *
 * @FieldFormatter(
 *   id = "semver_default",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "semver"
 *   }
 * )
 */
class SemverDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'prefix' => 'v',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['prefix'] = [
      '#type' => 'textfield',
      '#title' => t('Number Prefix'),
      '#default_value' => $this->getSetting('prefix'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $prefix = $this->getSetting('prefix');
    if ($prefix) {
      $summary[] = $this->t('Number prefix: "@prefix"', ['@prefix' => $prefix]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $prefix = $this->viewMode !== EntityDisplayBase::CUSTOM_MODE
        ? $this->getSetting('prefix')
        : '';
      $number = implode('.', [$item->major, $item->minor, $item->patch]);
      if (!empty($prefix)) {
        $number = $prefix . $number;
      }
      if (!empty($item->pre_release)) {
        $number .= '-' . $item->pre_release;
      }
      if (!empty($item->build)) {
        $number .= '+' . $item->build;
      }
      $elements[$delta] = [
        '#plain_text' => $number,
      ];
    }

    return $elements;
  }

}
