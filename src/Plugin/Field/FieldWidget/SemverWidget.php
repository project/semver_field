<?php

namespace Drupal\semver_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a default semver widget.
 *
 * @FieldWidget(
 *   id = "semver_default",
 *   label = @Translation("Semantic Version Number"),
 *   field_types = {
 *     "semver"
 *   }
 * )
 */
class SemverWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholders' => TRUE,
      'pre_release' => TRUE,
      'build' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['placeholders'] = [
      '#type' => 'checkbox',
      '#title' => t('Show placeholders in fields'),
      '#default_value' => $this->getSetting('placeholders'),
    ];
    $element['pre_release'] = [
      '#type' => 'checkbox',
      '#title' => t('Show pre-release version field'),
      '#default_value' => $this->getSetting('pre_release'),
    ];
    $element['build'] = [
      '#type' => 'checkbox',
      '#title' => t('Show build metadata field'),
      '#default_value' => $this->getSetting('build'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholders = $this->getSetting('placeholders');
    if (!empty($placeholders)) {
      $summary[] = t('Placeholders visible');
    }

    $pre_release = $this->getSetting('pre_release');
    if (!empty($pre_release)) {
      $summary[] = t('Pre-release version field visible');
    }

    $build = $this->getSetting('build');
    if (!empty($build)) {
      $summary[] = t('Build metadata field visible');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $placeholders = $this->getSetting('placeholders');

    $element['#type'] = 'fieldset';
    $element['#attached'] = [
      'library' => ['semver_field/widget'],
    ];

    $element['major'] = [
      '#type' => 'number',
      '#title' => $this->t('Major version'),
      '#title_display' => 'invisible',
      '#min' => 0,
      '#step' => 1,
      '#placeholder' => $placeholders ? $this->t('Major') : NULL,
      '#default_value' => $items[$delta]->major ?? NULL,
    ];

    $element['minor'] = [
      '#type' => 'number',
      '#title' => $this->t('Minor version'),
      '#title_display' => 'invisible',
      '#min' => 0,
      '#step' => 1,
      '#placeholder' => $placeholders ? $this->t('Minor') : NULL,
      '#default_value' => $items[$delta]->minor ?? NULL,
      '#field_prefix' => '.',
    ];

    $element['patch'] = [
      '#type' => 'number',
      '#title' => $this->t('Patch version'),
      '#title_display' => 'invisible',
      '#min' => 0,
      '#step' => 1,
      '#placeholder' => $placeholders ? $this->t('Patch') : NULL,
      '#default_value' => $items[$delta]->patch ?? NULL,
      '#field_prefix' => '.',
    ];

    if (!empty($this->getSetting('pre_release'))) {
      $element['pre_release'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pre-release version'),
        '#description' => '[0-9A-Za-z-.]+',
        '#title_display' => 'invisible',
        '#size' => 12,
        '#attributes' => ['pattern' => '[0-9A-Za-z-.]+'],
        '#placeholder' => $placeholders ? $this->t('Pre-release') : NULL,
        '#default_value' => $items[$delta]->pre_release ?? NULL,
        '#field_prefix' => '-',
      ];
    }

    if (!empty($this->getSetting('build'))) {
      $element['build'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Build metadata'),
        '#description' => '[0-9A-Za-z-.]+',
        '#title_display' => 'invisible',
        '#size' => 12,
        '#attributes' => ['pattern' => '[0-9A-Za-z-.]+'],
        '#placeholder' => $placeholders ? $this->t('Build') : NULL,
        '#default_value' => $items[$delta]->build ?? NULL,
        '#field_prefix' => '+',
      ];
    }

    return $element;
  }

}
