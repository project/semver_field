<?php

namespace Drupal\semver_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines the 'semver' field type.
 *
 * @FieldType(
 *   id = "semver",
 *   label = @Translation("Semantic Version Number"),
 *   description = @Translation("This field stores a semantic version number."),
 *   category = @Translation("Number"),
 *   default_widget = "semver_default",
 *   default_formatter = "semver_default"
 * )
 */
class SemverItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'major' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'minor' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'patch' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'pre_release' => [
          'type' => 'varchar',
          'length' => 512,
        ],
        'build' => [
          'type' => 'varchar',
          'length' => 512,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['major'] = DataDefinition::create('integer')
      ->setLabel(t('Major Version'))
      ->setRequired(TRUE);

    $properties['minor'] = DataDefinition::create('integer')
      ->setLabel(t('Minor Version'))
      ->setRequired(TRUE);

    $properties['patch'] = DataDefinition::create('integer')
      ->setLabel(t('Patch Version'))
      ->setRequired(TRUE);

    $properties['pre_release'] = DataDefinition::create('string')
      ->setLabel(t('Pre-release Version'))
      ->setDescription(t('Example "alpha.2", "beta", or "rc"'));

    $properties['build'] = DataDefinition::create('string')
      ->setLabel(t('Build Metadata'))
      ->setDescription(t('Example "alpha.2", "beta", or "rc'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['major'] = 2;
    $values['minor'] = 0;
    $values['patch'] = 0;
    $values['pre_release'] = 'beta.2';
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->major) && $this->major !== 0;
  }

}
