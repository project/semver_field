<?php

namespace Drupal\semver_field\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Basic sort handler for semver fields.
 *
 * @ViewsSort("semver")
 */
class Semver extends SortPluginBase {

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $this->ensureMyTable();
    // Order by field values.
    $this->query->addOrderBy($this->tableAlias, $this->field . '_major', $this->options['order']);
    $this->query->addOrderBy($this->tableAlias, $this->field . '_minor', $this->options['order']);
    $this->query->addOrderBy($this->tableAlias, $this->field . '_patch', $this->options['order']);
    // TODO: order by pre_release where versions with NULL pre_release are later than alpha/beta/etc.
  }

}
