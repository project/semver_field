# SemVer Field

Provides a field type for [semantic version numbers](https://semver.org/). Why not just use a textfield? For easier sorting/filtering in views, mostly.
