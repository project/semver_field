<?php

/**
 * @file
 * Provides views data for the semver_field module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function semver_field_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);
  if ($field_storage->getType() === 'semver') {
    foreach ($data as $table_name => $table_data) {
      // Set the 'semver' sort handler.
      $data[$table_name][$field_storage->getName()]['sort']['field'] = $field_storage->id();
      $data[$table_name][$field_storage->getName()]['sort']['id'] = 'semver';
    }
  }
  return $data;
}
